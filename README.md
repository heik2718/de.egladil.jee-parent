## de.egladil.jee-parent
This is just the parent pom used in my projects. It fixes the maven dependency and plugin versions and properties.

## Latest changes

21.09.2019: 5.1.0 - versions upgraded in order to fix a bunch of CVEs
